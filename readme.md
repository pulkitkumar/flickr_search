##The application consists of 2 modules.

1. ####app (Contains the search logic and UI)
2. ####image_loader (It has the api to load a url into image view. It also contains the Image Cache)

###The app is divided into 4 packages.

1. ####domain
    This has the business logic, the basic data objects and repository interfaces.
2. ####data
    This depends on domain layer and implements an interface from that to read from http api. 
3. ####presentation  
    This has a view model which is responsible for providing data to the UI. This layer uses domain layer interfaces to fetch and modify data.
4. ####UI
    This has the activity and the adapter. it is responsible for making calls to view model and showing the UI.
    
    
 