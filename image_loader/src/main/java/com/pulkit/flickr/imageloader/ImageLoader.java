package com.pulkit.flickr.imageloader;

import android.content.Context;
import android.widget.ImageView;

/**
 * This is the class to be used by users of this module.
 */
public class ImageLoader {

  /**
   * load a url for a given application context into an image.
   * context needed for {@link HasImageCache} from the application class.
   */
  public static void load(Context context, ImageView imageView, String url) {
    ImageLoaderTask.attachTask(context, imageView, url);
  }

}
