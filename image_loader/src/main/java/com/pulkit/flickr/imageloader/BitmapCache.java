package com.pulkit.flickr.imageloader;

import android.graphics.Bitmap;
import android.os.Build.VERSION;
import android.os.Build.VERSION_CODES;
import android.util.LruCache;

/**
 * This is a wrapper over {@link LruCache} to hold bitmaps for this application.
 */
public class BitmapCache {

  private final LruCache<String, Bitmap> cache;

  private BitmapCache(int cacheSize) {
    this.cache = new LruCache<>(cacheSize);
  }

  public boolean contains(String url) {
    if (cache.get(url) == null) {
      return false;
    } else {
      return true;
    }
  }

  public Bitmap getBitmap(String url) {
    return cache.get(url);
  }

  public void put(String url, Bitmap bitmap) {
    cache.put(url, bitmap);
  }

  // trim size in case of low memory.
  public void trimToSize(int size) {
    if (VERSION.SDK_INT >= VERSION_CODES.JELLY_BEAN_MR1) {
      cache.trimToSize(size);
    } else {
      cache.evictAll();
    }
  }

  public static BitmapCache create(int maxSize) {
    return new BitmapCache(maxSize);
  }
}
