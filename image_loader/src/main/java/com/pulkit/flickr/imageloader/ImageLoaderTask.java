package com.pulkit.flickr.imageloader;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;
import java.io.IOException;
import java.lang.ref.WeakReference;

/**
 * This is an async task responsible for loading a bitmap into a given ImageView.
 */
public class ImageLoaderTask extends AsyncTask<Void, Void, Bitmap> {

  private static final String TAG = ImageLoaderTask.class.getSimpleName();

  private final WeakReference<ImageView> imageViewReference;
  private final String url;
  private final BitmapCache imageCache;

  ImageLoaderTask(BitmapCache imageCache, ImageView imageView, String url) {
      this.imageCache = imageCache;
      this.url = url;
      this.imageViewReference = new WeakReference<>(imageView);
  }

  @Override
  protected void onPreExecute() {
    super.onPreExecute();
    ImageView imageView = imageViewReference.get();
    if (imageView != null) { // set place holder image before starting download.
      imageView.setImageResource(R.drawable.ic_placeholder);
    }
  }

  @Override
  protected Bitmap doInBackground(Void... strings) {
    // read from cache if it is already present.
    if (imageCache != null && imageCache.contains(url)) {
      Log.i(TAG, "image loaded from cache for "+ url);
      return imageCache.getBitmap(url);
    } else {
      // download if it is not in the cache and put it in the cache.
      try {
        Bitmap bitmap = ImageDownloader.getInstance().download(url);
        Log.i(TAG, "image downloaded for "+ url);
        if (imageCache != null) {
          imageCache.put(url, bitmap);
        }
        return bitmap;
      } catch (IOException e) {
        return null;
      }
    }
  }

  @Override
  protected void onPostExecute(Bitmap bitmap) {
    super.onPostExecute(bitmap);
    if (!isCancelled()) { // ignore loading if task cancelled already.
      ImageView imageView = imageViewReference.get();
      if (imageView != null) {
        // to check if a new task is not attached to the image view.
        if (imageView.getTag(R.string.load_task) == this) {
          imageView.setImageBitmap(bitmap);
        }
      }
    }
  }

  public static void attachTask(Context context, ImageView imageView, String url) {
    BitmapCache imageCache = null;
    if (context.getApplicationContext() instanceof HasImageCache) {
      imageCache = ((HasImageCache) context.getApplicationContext()).imageCache();
    }

    // cancel existing tasks.
    if (imageView.getTag(R.string.load_task) != null) {
      ImageLoaderTask task = (ImageLoaderTask) imageView.getTag(R.string.load_task);
      if (task.getStatus() != Status.FINISHED && !task.isCancelled()) {
        task.cancel(false);
      }
    }

    ImageLoaderTask task = new ImageLoaderTask(imageCache, imageView, url);
    imageView.setTag(R.string.load_task, task); // add task as tag for retrieving in future for cancellation (if the image view is recycled).
    task.execute();
  }
}
