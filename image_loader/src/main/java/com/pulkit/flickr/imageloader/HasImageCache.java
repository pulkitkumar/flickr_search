package com.pulkit.flickr.imageloader;

/**
 * To be implemented by the application class.
 * As application class should hold the reference to the cache so that it is not collected in gc.
 */
public interface HasImageCache {

  BitmapCache imageCache();

}
