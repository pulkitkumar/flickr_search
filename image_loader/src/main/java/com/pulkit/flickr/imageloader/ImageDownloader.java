package com.pulkit.flickr.imageloader;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * This is a singleton responsible for downloading an image from a http url into a Bitmap.
 */
public class ImageDownloader {

  private ImageDownloader() { }

  public Bitmap download(String src) throws IOException {
    HttpURLConnection connection = null;
    InputStream inputStream = null;
    try {
      URL url = new URL(src);
      connection = (HttpURLConnection) url.openConnection();
      connection.setDoInput(true);
      connection.connect();
      inputStream = connection.getInputStream();
      Bitmap myBitmap = BitmapFactory.decodeStream(inputStream);
      return myBitmap;
    } finally {
      if (connection != null) {
        connection.disconnect();
      }
      if (inputStream != null) {
        inputStream.close();
      }
    }
  }

  static ImageDownloader imageDownloader = null;

  static ImageDownloader getInstance() {
    if (imageDownloader == null) {
      return new ImageDownloader();
    } else {
      return imageDownloader;
    }
  }
}
