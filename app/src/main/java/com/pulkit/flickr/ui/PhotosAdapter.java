package com.pulkit.flickr.ui;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.pulkit.flickr.R;
import com.pulkit.flickr.imageloader.ImageLoader;
import java.util.ArrayList;
import java.util.List;

class PhotosAdapter extends RecyclerView.Adapter<PhotosAdapter.ViewHolder> {

  private final List<String> photos;
  private final Context context;

  public PhotosAdapter(Context context) {
    this.context = context;
    this.photos = new ArrayList<>();
  }

  public void add(List<String> flickrPhotos) {
    photos.addAll(flickrPhotos);
  }

  public void clear() {
    photos.clear();
  }

  public List<String> getItems() {
    return photos;
  }

  public static class ViewHolder extends RecyclerView.ViewHolder {

    public final ImageView imageView;

    public ViewHolder(@NonNull View itemView) {
      super(itemView);
      imageView = itemView.findViewById(R.id.image);
    }
  }

  @NonNull
  @Override
  public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
    View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_photo, viewGroup, false);
    return new ViewHolder(view);
  }

  @Override
  public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
    ImageLoader.load(context, viewHolder.imageView, photos.get(position));
  }

  @Override
  public int getItemCount() {
    return photos.size();
  }
}
