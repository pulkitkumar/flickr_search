package com.pulkit.flickr.ui;

import android.app.Activity;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BaseTransientBottomBar;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;
import com.pulkit.flickr.R;
import com.pulkit.flickr.presentation.FlickrSearchViewModel;
import com.pulkit.flickr.presentation.FlickrSearchViewModel.FlickrSearchViewModelFactory;

/**
 * main search activity.
 */
public class FlickrSearchActivity extends AppCompatActivity {

  private static final String KEY_QUERY = "KeyQuery"; // to save in out state.

  private FlickrSearchViewModel viewModel;
  private PhotosAdapter adapter;
  private String query; // saves the recently searched text.

  private SearchView search;
  private EndlessScrollListener scrollListener;

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    if (savedInstanceState != null) {
      query = savedInstanceState.getString(KEY_QUERY); // load from out state in case of screen rotation.
    }

    // initialize view model.
    FlickrSearchViewModelFactory factory = new FlickrSearchViewModelFactory(
        getApplicationContext());
    viewModel = ViewModelProviders.of(this, factory).get(FlickrSearchViewModel.class);

    // set layout and initialize ui references.
    setContentView(R.layout.activity_search);
    search = findViewById(R.id.searchView);
    final RecyclerView recyclerView = findViewById(R.id.recyclerView);

    // set up recycler view.
    adapter = new PhotosAdapter(this);
    GridLayoutManager layoutManager = new GridLayoutManager(this, 3);
    recyclerView.setLayoutManager(layoutManager);
    recyclerView.setAdapter(adapter);
    scrollListener = new EndlessScrollListener(layoutManager) { // for end of page callback/
      @Override
      public void onLoadMore(int page) {
        viewModel.search(query, page);
      }
    };
    recyclerView.addOnScrollListener(scrollListener);

    search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
      @Override
      public boolean onQueryTextSubmit(String s) {
        query = s;
        scrollListener.reset();
        viewModel.search(query, EndlessScrollListener.INITIAL_PAGE);
        hideKeyboard(FlickrSearchActivity.this, search);
        return true;
      }

      @Override
      public boolean onQueryTextChange(String s) { // not searching while user is typing, searching only when search is clicked.
        return false;
      }
    });

    // observe to live data.
    viewModel.urls.observe(this, urls -> {
      int oldPhotosSize = adapter.getItems().size();
      adapter.clear();
      adapter.add(urls);
      if (urls.size() > 0) { // only notify items that are added in case of pagination.
        adapter.notifyItemRangeInserted(oldPhotosSize, urls.size() - oldPhotosSize);
      } else { // in case of new search we can notify whole data set change.
        adapter.notifyDataSetChanged();
      }
    });

    viewModel.error.observe(this, s -> // showing a snackbar in case of error.
        Snackbar.make(search, s, BaseTransientBottomBar.LENGTH_SHORT).show());
    viewModel.progress.observe(this, progress -> {
          if (progress) { // showing a toast whenever progress is true.
            Toast.makeText(this, getString(R.string.loading), Toast.LENGTH_SHORT).show();
          }
        });
  }

  @Override
  protected void onResume() {
    super.onResume();
    search.clearFocus();
  }

  @Override
  protected void onSaveInstanceState(Bundle outState) {
    outState.putString(KEY_QUERY, query);
    super.onSaveInstanceState(outState);
  }

  public static void hideKeyboard(Context context, View view) {
    InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
  }

}
