package com.pulkit.flickr;

import android.app.Application;
import com.pulkit.flickr.imageloader.BitmapCache;
import com.pulkit.flickr.imageloader.HasImageCache;

/**
 * Application class for the app.
 */
public class FlickrApplication extends Application implements HasImageCache {

  // holds a singleton reference to the image cache.
  private BitmapCache imageCache;

  @Override
  public void onCreate() {
    super.onCreate();
    final int cacheSize = getCacheSize();
    // create image cache on app launch.
    this.imageCache = BitmapCache.create(cacheSize);
  }

  @Override
  public void onTrimMemory(int level) {
    super.onTrimMemory(level);
    // trim image cache size if memory for the app is low.
    if (level == TRIM_MEMORY_RUNNING_LOW || level == TRIM_MEMORY_RUNNING_CRITICAL) {
      imageCache.trimToSize(getCacheSize() / 4);
    }
  }

  @Override
  public BitmapCache imageCache() {
    return imageCache;
  }

  // 1/4th of available memory for image cache. (A big cache size as the app is bitmap intensive.)
  private static int getCacheSize() {
    final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
    return maxMemory / 4;
  }

}
