package com.pulkit.flickr.data;

import static android.content.ContentValues.TAG;

import android.util.Log;
import com.pulkit.flickr.domain.FlickrPhoto;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * This is a singleton class which has the logic to convert a json string to {@link FlickrPhoto} object.
 */
public class FlickrJsonObjectMapper {

  private static FlickrJsonObjectMapper instance;

  public static FlickrJsonObjectMapper getInstance() {
    if (instance == null) {
      instance = new FlickrJsonObjectMapper();
    }
    return instance;
  }

  private FlickrJsonObjectMapper() { }

  public List<FlickrPhoto> map(String jsonString) {
    try {
      JSONObject jsonObject = new JSONObject(jsonString);
      List<FlickrPhoto> photos = new ArrayList<>();
      if (jsonObject.has("photos")) {
        jsonObject = jsonObject.getJSONObject("photos");
        if (jsonObject.has("photo")) {
          JSONArray array = jsonObject.getJSONArray("photo");
          for (int i=0; i<array.length(); i++) {
            JSONObject photoJson = array.getJSONObject(i);
            FlickrPhoto photo = parsePhoto(photoJson);
            if (photo != null) {
              photos.add(photo);
            }
          }
        }
      }
      return photos;
    } catch (JSONException e) {
      Log.wtf(TAG, e);
      return null;
    }
  }

  // maps one json object to one FlickrPhoto.
  private FlickrPhoto parsePhoto(JSONObject photoJson) {
    try {
      String id = photoJson.getString("id");
      String owner = photoJson.getString("owner");
      String secret = photoJson.getString("secret");
      String server = photoJson.getString("server");
      int farm = photoJson.getInt("farm");
      String title = photoJson.getString("title");
      boolean ispublic = photoJson.getInt("ispublic") != 0;
      boolean isfriend = photoJson.getInt("isfriend") != 0;
      boolean isfamily = photoJson.getInt("isfamily") != 0;
      return new FlickrPhoto(id, owner, secret, server, farm, title, ispublic, isfriend, isfamily);
    } catch (JSONException e) {
      return null;
    }
  }
}
