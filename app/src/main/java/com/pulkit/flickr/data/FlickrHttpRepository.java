package com.pulkit.flickr.data;

import com.pulkit.flickr.domain.FlickrPhoto;
import com.pulkit.flickr.domain.FlickrRepository;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

/**
 * This is a singleton class responsible for making photos search http api call.
 */
public class FlickrHttpRepository implements FlickrRepository {

  private static FlickrRepository instance;
  private static final String API_KEY = "3e7cc266ae2b0e0d78e279ce8e361736";

  public static FlickrRepository getInstance() {
    if (instance == null) {
      instance = new FlickrHttpRepository(API_KEY, FlickrJsonObjectMapper.getInstance());
    }
    return instance;
  }

  private static final String PROTOCOL = "https://";
  private static final String HOST = "api.flickr.com";
  private static final String PATH = "/services/rest/";
  private static final String DEFAULT_PARAMS = "method=flickr.photos.search&format=json&nojsoncallback=1&safe_search=1";
  private static final String PARAM_API_KEY = "api_key=";
  private static final String PARAM_SEARCH_TEXT = "text=";
  private static final String PARAM_PATH_SEPARATOR = "?";
  private static final String PARAM_SEPARATOR = "&";
  private static final String PARAM_PAGE = "page=";

  private final String url; // holds a base url for search.
  private final FlickrJsonObjectMapper mapper;

  private FlickrHttpRepository(String apiKey, FlickrJsonObjectMapper mapper) {
    this.url = createUrl(apiKey);
    this.mapper = mapper;
  }

  // creates base url for search based on the API KEY.
  private String createUrl(String apiKey) {
    return new StringBuilder(PROTOCOL)
        .append(HOST)
        .append(PATH)
        .append(PARAM_PATH_SEPARATOR).append(DEFAULT_PARAMS)
        .append(PARAM_SEPARATOR).append(PARAM_API_KEY).append(apiKey)
        .toString();
  }

  // creates url for a search text and page number using base url.
  private String formUrlForSearch(String text, int page) {
    return new StringBuilder(url)
        .append(PARAM_SEPARATOR).append(PARAM_SEARCH_TEXT).append(text)
        .append(PARAM_SEPARATOR).append(PARAM_PAGE).append(String.valueOf(page))
        .toString();
  }

  @Override
  public List<FlickrPhoto> search(String searchTerm, int page) throws IOException {
    URL url = new URL(formUrlForSearch(searchTerm, page));
    HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
    InputStream in = null;
    urlConnection.setRequestMethod("GET");
    try {
      in = new BufferedInputStream(urlConnection.getInputStream());
      String data = readStream(in);
      List<FlickrPhoto> photos = mapper.map(data);
      return photos;
    } finally {
      urlConnection.disconnect();
      if (in != null) {
        in.close();
      }
    }
  }

  private String readStream(InputStream in) throws IOException {
    BufferedReader br = new BufferedReader(new InputStreamReader(in, "utf8"));
    StringBuffer sb = new StringBuffer();
    String line;

    while ((line = br.readLine()) != null) {
      sb.append(line);
    }

    return sb.toString();
  }
}
