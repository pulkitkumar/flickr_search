package com.pulkit.flickr.presentation;

import static com.pulkit.flickr.ui.EndlessScrollListener.INITIAL_PAGE;

import android.app.Application;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider.Factory;
import android.content.Context;
import android.os.AsyncTask;
import android.os.AsyncTask.Status;
import android.support.annotation.NonNull;
import com.pulkit.flickr.R;
import com.pulkit.flickr.data.FlickrHttpRepository;
import com.pulkit.flickr.domain.FlickrPhoto;
import com.pulkit.flickr.domain.FlickrRepository;
import com.pulkit.flickr.domain.FlickrUrlFramer;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * View model for {@link com.pulkit.flickr.ui.FlickrSearchActivity}
 */
public class FlickrSearchViewModel extends ViewModel {

  private final Application context;
  private final FlickrRepository repository;
  private FlickrSearchTask task;
  private String prevSearchTerm;

  public final MutableLiveData<List<String>> urls; // this is the list of all urls for the current query.
  public final MutableLiveData<Boolean> progress; // changes to true when we need to show progress.
  public final MutableLiveData<String> error; // changes to the error string.

  private FlickrSearchViewModel(Application context, FlickrRepository repository) {
    this.context = context;
    this.repository = repository;
    this.urls = new MutableLiveData<>();
    urls.setValue(new ArrayList<>());
    this.progress = new MutableLiveData<>();
    this.error = new MutableLiveData<>();
  }

  // to be called with search term and the page number.
  public void search(String searchTerm, int page) {
    // cancel already running tasks if any.
    if (task != null) {
      if (task.getStatus() != Status.FINISHED && !task.isCancelled()) {
        task.cancel(false);
      }
    }

    // clear urls if the search term is different from the previous one. or if it is the first page.
    if ((prevSearchTerm != null && !prevSearchTerm.equals(searchTerm)) || page == INITIAL_PAGE) {
       List<String> previous = urls.getValue();
       previous.clear();
       urls.setValue(previous);
    }

    // start task.
    task = new FlickrSearchTask();
    task.execute(searchTerm, String.valueOf(page));
    prevSearchTerm = searchTerm;
  }

  public class FlickrSearchTask extends AsyncTask<String, Void, List<String>> {

    @Override
    protected void onPreExecute() {
      super.onPreExecute();
      progress.setValue(true);
    }

    @Override
    protected List<String> doInBackground(String... strings) {
      try {
        List<FlickrPhoto> photos = repository.search(strings[0], Integer.parseInt(strings[1]));
        if (photos != null) {
          List<String> urls = new ArrayList<>();
          for (FlickrPhoto photo: photos) {
            urls.add(FlickrUrlFramer.getInstance().frameUrl(photo));
          }
          return urls;
        } else {
          return null;
        }
      } catch (IOException e) {
        error.postValue(context.getString(R.string.load_error));
        return null;
      }
    }

    @Override
    protected void onPostExecute(List<String> urls) {
      super.onPostExecute(urls);
      List<String> allUrls = FlickrSearchViewModel.this.urls.getValue();
      allUrls.addAll(urls);
      progress.setValue(false);
      FlickrSearchViewModel.this.urls.setValue(allUrls);
    }
  }

  // Factory to create view model.
  public static class FlickrSearchViewModelFactory implements Factory {

    private final Application context;

    public FlickrSearchViewModelFactory(Context context) {
      this.context = (Application) context.getApplicationContext();
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
      if (modelClass.isAssignableFrom(FlickrSearchViewModel.class)) {
        return (T) new FlickrSearchViewModel(context, FlickrHttpRepository.getInstance());
      } else {
        throw new IllegalStateException("Can not assign view model for "+ modelClass.getName());
      }
    }
  }

}
