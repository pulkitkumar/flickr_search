package com.pulkit.flickr.domain;

import java.io.IOException;
import java.util.List;

/**
 * Search Repository interface for photos.
 */
public interface FlickrRepository {

  List<FlickrPhoto> search(String searchTerm, int i) throws IOException;
}
