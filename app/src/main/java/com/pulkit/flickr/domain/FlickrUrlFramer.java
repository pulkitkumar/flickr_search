package com.pulkit.flickr.domain;

/**
 * This class is a singleton and contains the logic to convert
 * a {@link FlickrPhoto} object to a http url.
 */
public class FlickrUrlFramer {

  public static final String URL_FORMAT = "http://farm%s.static.flickr.com/%s/%s_%s.jpg";

  private static FlickrUrlFramer instance = null;

  public static FlickrUrlFramer getInstance() {
    if (instance == null) {
      instance = new FlickrUrlFramer();
    }
    return instance;
  }

  private FlickrUrlFramer() { }

  public String frameUrl(FlickrPhoto photo) {
    return String.format(URL_FORMAT, String.valueOf(photo.farm), photo.server, photo.id, photo.secret);
  }

}
