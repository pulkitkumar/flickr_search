package com.pulkit.flickr.domain;

/**
 * Data class to hold the FlickrPhoto domain object.
 */
public class FlickrPhoto {

  public final String id;
  public final String owner;
  public final String secret;
  public final String server;
  public final int farm;
  public final String title;
  public final boolean ispublic;
  public final boolean isfriend;
  public final boolean isfamily;

  public FlickrPhoto(String id, String owner, String secret, String server, int farm,
      String title, boolean ispublic, boolean isfriend, boolean isfamily) {
    this.id = id;
    this.owner = owner;
    this.secret = secret;
    this.server = server;
    this.farm = farm;
    this.title = title;
    this.ispublic = ispublic;
    this.isfriend = isfriend;
    this.isfamily = isfamily;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    FlickrPhoto that = (FlickrPhoto) o;
    return this.id.equals(that.id);
  }

  @Override
  public int hashCode() {
    return this.id.hashCode();
  }

  @Override
  public String toString() {
    return "FlickrPhoto{" +
        "id='" + id + '\'' +
        ", owner='" + owner + '\'' +
        ", secret='" + secret + '\'' +
        ", server='" + server + '\'' +
        ", farm=" + farm +
        ", title='" + title + '\'' +
        ", ispublic=" + ispublic +
        ", isfriend=" + isfriend +
        ", isfamily=" + isfamily +
        '}';
  }
}
