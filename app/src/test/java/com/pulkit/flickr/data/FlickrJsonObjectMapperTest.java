package com.pulkit.flickr.data;

import com.pulkit.flickr.domain.FlickrPhoto;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

@RunWith(RobolectricTestRunner.class)
public class FlickrJsonObjectMapperTest {

  FlickrPhoto actual = new FlickrPhoto("41511243574", "96656936@N06", "18d358bef8", "959", 1, "2018-05-19-BBHHS-prom-001", true, false, false);

  @Test
  public void testParsing() throws IOException {
    String json = readJsonFromFile();
    List<FlickrPhoto> photos = FlickrJsonObjectMapper.getInstance().map(json);

    Assert.assertEquals(photos.size(), 100);

    Assert.assertEquals(photos.get(0), actual);
  }

  private String readJsonFromFile() throws IOException {

    InputStream inputStream = null;
    try {
      inputStream = this.getClass().getClassLoader().getResourceAsStream("test.json");
      System.out.println(inputStream);
      int size = inputStream.available();
      byte[] buffer = new byte[size];
      inputStream.read(buffer);
      inputStream.close();
      String json = new String(buffer, "UTF-8");
      return json;
    } finally {
      if (inputStream != null) {
        inputStream.close();
      }
    }
  }

}
