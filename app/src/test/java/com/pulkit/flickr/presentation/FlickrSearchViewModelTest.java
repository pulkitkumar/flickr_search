package com.pulkit.flickr.presentation;

import static com.pulkit.flickr.domain.MockRepository.SAMPLE_SIZE;
import static org.junit.Assert.assertEquals;

import com.pulkit.flickr.domain.FlickrRepository;
import com.pulkit.flickr.domain.MockRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;

@RunWith(RobolectricTestRunner.class)
public class FlickrSearchViewModelTest {

  FlickrRepository repository;
  FlickrSearchViewModel viewModel;

  @Before
  public void setup() {
    repository= new MockRepository();
    viewModel = new FlickrSearchViewModel(RuntimeEnvironment.application, repository);
  }

  @Test
  public void test_search_same_text_doesnot_clear_live_data() {

    viewModel.search("test", 1);

    assertEquals(viewModel.urls.getValue().size(), SAMPLE_SIZE);

    viewModel.search("test", 2);

    assertEquals(viewModel.urls.getValue().size(), 2* SAMPLE_SIZE);
    assert viewModel.error.getValue() == null;
  }

  @Test
  public void test_search_new_text_clear_live_data() {
    viewModel.search("test1", 1);

    assertEquals(viewModel.urls.getValue().size(), SAMPLE_SIZE);
    viewModel.search("test2", 1);

    assertEquals(viewModel.urls.getValue().size(), SAMPLE_SIZE);
    assert viewModel.error.getValue() == null;
  }

}
