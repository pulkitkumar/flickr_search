package com.pulkit.flickr.domain;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

@RunWith(RobolectricTestRunner.class)
public class FlickrUrlFramerTest {

  @Test
  public void checkUrlFraming() {
    FlickrPhoto photo = new FlickrPhoto("41511243574", "96656936@N06", "18d358bef8", "959", 1, "2018-05-19-BBHHS-prom-001", true, false, false);
    String url = FlickrUrlFramer.getInstance().frameUrl(photo);
    assertEquals(url, "http://farm1.static.flickr.com/959/41511243574_18d358bef8.jpg");
  }

}
