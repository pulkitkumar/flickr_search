package com.pulkit.flickr.domain;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MockRepository implements FlickrRepository {

  public static int SAMPLE_SIZE = 5;

  @Override
  public List<FlickrPhoto> search(String searchTerm, int i) throws IOException {
    ArrayList<FlickrPhoto> photos = new ArrayList<>();
    FlickrPhoto photo1 = new FlickrPhoto("1", "96656936@N06", "18d358bef8", "959", 1, "2018-05-19-BBHHS-prom-001", true, false, false);
    FlickrPhoto photo2 = new FlickrPhoto("2", "96656936@N06", "18d358bef8", "959", 1, "2018-05-19-BBHHS-prom-001", true, false, false);
    FlickrPhoto photo3 = new FlickrPhoto("3", "96656936@N06", "18d358bef8", "959", 1, "2018-05-19-BBHHS-prom-001", true, false, false);
    FlickrPhoto photo4 = new FlickrPhoto("4", "96656936@N06", "18d358bef8", "959", 1, "2018-05-19-BBHHS-prom-001", true, false, false);
    FlickrPhoto photo5 = new FlickrPhoto("5", "96656936@N06", "18d358bef8", "959", 1, "2018-05-19-BBHHS-prom-001", true, false, false);
    photos.add(photo1);
    photos.add(photo2);
    photos.add(photo3);
    photos.add(photo4);
    photos.add(photo5);
    return photos;
  }
}
